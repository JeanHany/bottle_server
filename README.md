# Bottle Server #

This repository is use to send mail to a user.

We use bottle for this server

you can launch the server with this commande :

$nohup uwsgi --socket 127.0.0.1:3031 --wsgi-file bottle_server.py --uid www-data --gid www-data --master --processes 1 --threads 2 &

The config for the nginx server is in the file bottle
