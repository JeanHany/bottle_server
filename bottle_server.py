from bottle import get, post, request, run, hook, response, route, default_app # or route
from elasticsearch import Elasticsearch
import smtplib, ssl
import json
import certifi

@post('/send_mail') # or @route('/login')
def send_mail():
    port = 587  # For starttls
    smtp_server = YOUR_SMTP_SERVER
    sender_email = YOUR_MAIL
    dict_cookies = request.cookies
    data_json = request.json
    receiver_email = data_json["email"]
    data_list = data_json["comment"]
    password = YOUR_PASSWORD
    users = ""
    pubs = ""
    teams = ""
    your_search = []
    your_result = []
    your_query = ""
    msg = "To: "+ receiver_email+"\r\n"
    msg += "Subject: Your basket from lookinlabs \r \n"
    msg += "Hi, this is your basket items from LookinLabs!" + '\n' + '\n'
    for i in range(0, len(data_list)) :
        your_query = ""
        search_user = data_list[i]['search']
        search_user = search_user.replace('.', ' ')
        if search_user in your_search :
            your_query = your_result[your_search.index(search_user)]
        else :
            your_search.append(search_user)
        found = False
        if data_list[i]['type'] == 'individual' :
            info_indiv = getinfoindiv(data_list[i]['id'])
            if info_indiv['found'] :
                data_list[i]['name'] = info_indiv["_source"]['forename']+' '+info_indiv["_source"]['surname']
                data_list[i]['link'] =  YOUR_SITE_URL +'/individual/'+ data_list[i]['id']
                found = True
        if data_list[i]['type'] == 'team' :
            info_team = getinfoteam(data_list[i]['id'])
            if info_team['found'] :
                data_list[i]['name'] = info_team["_source"]['acronym']
                data_list[i]['link'] = YOUR_SITE_URL +'/team/'+ data_list[i]['id']
                found = True
        if data_list[i]['type'] == 'pub' :
            info_pub = getinfopub(data_list[i]['id'])
            if info_pub['found'] :
                if info_pub["_source"]['title_en'] == "" :
                    if info_pub["_source"]['pdflink'] != "" :
                        data_list[i]['name'] = info_pub["_source"]['title_fr']
                        data_list[i]['link'] = info_pub["_source"]['pdflink']
                    else :
                        data_list[i]['name'] = info_pub["_source"]['title_fr']
                        data_list[i]['link'] = info_pub["_source"]['hallink']
                else :
                    if info_pub["_source"]['pdflink'] != "" :
                        data_list[i]['name'] = info_pub["_source"]['title_en']
                        data_list[i]['link'] = info_pub["_source"]['pdflink']
                    else :
                        data_list[i]['name'] = info_pub["_source"]['title_en']
                        data_list[i]['link'] = info_pub["_source"]['hallink']
                found = True
        if found :
            your_query = your_query + "" + data_list[i]['name'] +": " +data_list[i]['link']+"\n"
            if your_search.index(search_user) > len(your_result)-1 :
                your_result.append(your_query)
            else :
                your_result[your_search.index(search_user)] = your_query
    for i in range(0, len(your_result)) :
        if your_search[i] != "" and your_search[i] is not None :
            msg = msg + "Your query : "+ your_search[i] + "\n\n" + your_result[i] + '\n'
        else :
            msg = msg + "Other results save : \n\n" + your_result[i] + '\n'
    msg += "Thank you for using LookinLabs for your search."
    context = ssl.create_default_context()
    with smtplib.SMTP(smtp_server, port) as server:
        server.starttls(context=context)
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, msg.encode('utf-8'))

def getinfoindiv(data_id) :
    es = Elasticsearch("localhost:9200", ca_certs=certifi.where())
    return es.get(index="nested_inria_author_update", doc_type="nestedAuthor", id=data_id)

def getinfoteam(data_id) :
    es = Elasticsearch("localhost:9200", ca_certs=certifi.where())
    return es.get(index="nested_researchteam_inria_update", doc_type="nestedResearchteam", id=data_id)

def getinfopub(data_id) :
    es = Elasticsearch("localhost:9200", ca_certs=certifi.where())
    return es.get(index="publications_inria_update", doc_type="publication", id=data_id)

if __name__ == '__main__' :
    run(host='localhost', port='3031')
else :
    application = default_app()
